// GUIManager.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "TextureManager.hpp"
#include "ServiceLocator.hpp"
#include "GUIManager.hpp"

#include "GUILabel.hpp"

namespace spaceshooter
{
	GUIManager::GUIManager()
	{
		m_texture = nullptr;
	}

	GUIManager::~GUIManager()
	{
		
	}

	bool GUIManager::Initialize(const std::string& filename)
	{
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->RegisterMouseEventListener(this, &GUIManager::OnMouse);



		return true;
	}

	void GUIManager::Shutdown()
	{
		auto itrWidget = m_widgets.begin();
		while (itrWidget != m_widgets.end())
		{
			delete (*itrWidget);
			++itrWidget;
		}
		m_widgets.clear();

		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->UnregisterMouseEventListener(this);
	}

	void GUIManager::Update()
	{
		/*
		if (!m_dirty)
			return;

		m_dirty = false;
		m_vertex_array.clear();

		for (unsigned int index = 0; index < m_widgets.size(); index++)
		{
			if (m_widgets[index]->IsVisible())
			{
				m_widgets[index]->OnDraw(m_vertex_array);
			}
		}
		*/
	}

	void GUIManager::Draw(DrawManager* draw_manager)
	{
		/*
		if (m_texture)
		{
			sf::RenderStates states;
			states.texture = m_texture;
			draw_manager->Draw(m_vertex_array, states);
		}

		for (unsigned int index = 0; index < m_widgets.size(); index++)
		{
			if (m_widgets[index]->HasText())
			{
				GUILabel* label = static_cast<GUILabel*>(m_widgets[index]);
				draw_manager->Draw(label->GetText());
			}
		}
		*/
	}

	void GUIManager::AttachWidget(GUIWidget* widget)
	{
		widget->RegisterWidgetCallback(this, &GUIManager::OnChange);
		m_widgets.push_back(widget);
		m_dirty = true;
	}

	// private
	void GUIManager::OnMouse(const MouseEvent& event)
	{
	}

	void GUIManager::OnChange(const GUIWidget* widget)
	{
	}

	
} // namespace spaceshooter
