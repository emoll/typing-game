// GUIWordBox.cpp

#include "stdafx.h"
#include "GUIWordBox.hpp"

namespace spaceshooter
{
	GUIWordBox::GUIWordBox(sf::Font* font)
		: GUIWidget(EWidgetType::WordBox)
	{
		m_has_text = true;
		m_text.setFont(*font);
	}

	GUIWordBox::~GUIWordBox()
	{
	}

	void GUIWordBox::OnDraw(sf::VertexArray& vertex_array)
	{
		// void
	}

	void GUIWordBox::SetTextString(const std::string& text)
	{
		m_text.setString(text);
	}

	void GUIWordBox::SetTextColor(const sf::Color& color)
	{
		m_text.setColor(color);
	}

	void GUIWordBox::SetTextSize(unsigned int size)
	{
		m_text.setCharacterSize(size);
	}

	void GUIWordBox::SetTextPosition(float x, float y)
	{
		m_rect.left = x;
		m_rect.top = y;
		m_text.setPosition(x, y);
	}

	void GUIWordBox::SetTextPosition(const sf::Vector2f& position)
	{
		m_rect.left = position.x;
		m_rect.top = position.y;
		m_text.setPosition(position.x, position.y);
	}

	const sf::Text& GUIWordBox::GetText() const
	{
		return m_text;
	}
} // namespace spaceshooter
