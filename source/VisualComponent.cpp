// VisualComponent.cpp

#include "stdafx.h"
#include "VisualComponent.hpp"

namespace spaceshooter
{
	VisualComponent::VisualComponent(const sf::Texture& texture)
		: m_sprite(texture)
	{
	}

	VisualComponent::~VisualComponent()
	{
	}

	EComponentType VisualComponent::GetType() const
	{
		return EComponentType::VisualComponent;
	}

	const sf::Sprite& VisualComponent::GetSprite() const
	{
		return m_sprite;
	}
} // namespace spaceshooter
