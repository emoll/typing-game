// ParticleTestState.cpp

#include "stdafx.h"
#include "TextureManager.hpp"
#include "ServiceLocator.hpp"
#include "ParticleEffectManager.hpp"
#include "ParticleTestState.hpp"

#include "ParticleEffect.hpp"
#include "FireParticleEmitter.hpp"

namespace spaceshooter
{
	ParticleTestState::ParticleTestState()
	{
		m_particle_effect_manager = nullptr;
	}

	ParticleTestState::~ParticleTestState()
	{
	}

	bool ParticleTestState::Enter()
	{
		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

		sf::Texture* texture = texture_manager->CreateTextureFromFile("../assets/art/cloud.png");

		FireParticleEmitter* emitter = new FireParticleEmitter(texture);
		emitter->SetPosition(sf::Vector2f(300.0f, 200.0f));

		ParticleEffect* effect = new ParticleEffect;
		effect->AddParticleEmitter(emitter);

		m_particle_effect_manager = new ParticleEffectManager;
		m_particle_effect_manager->AttachParticleEffect(effect);

		return true;
	}

	void ParticleTestState::Exit()
	{
		if (m_particle_effect_manager)
		{
			delete m_particle_effect_manager;
			m_particle_effect_manager = nullptr;
		}
	}

	bool ParticleTestState::Update(float deltatime)
	{
		m_particle_effect_manager->Update(deltatime);

		return true;
	}

	void ParticleTestState::Draw()
	{
		m_particle_effect_manager->Draw();
	}

	std::string ParticleTestState::GetNextState()
	{
		return std::string("");
	}
} // namespace spaceshooter
