// InputManager.cpp

#include "stdafx.h"
#include "InputManager.hpp"

namespace spaceshooter
{
	InputManager::InputManager()
	{
	}

	InputManager::~InputManager()
	{
	}

	bool InputManager::Initialize()
	{
		// note(tommi): here we set default keys to actions
		MapKeyToAction(sf::Keyboard::A, EActionType::Left);
		MapKeyToAction(sf::Keyboard::D, EActionType::Right);
		MapKeyToAction(sf::Keyboard::W, EActionType::Up);
		MapKeyToAction(sf::Keyboard::S, EActionType::Down);
		MapKeyToAction(sf::Keyboard::Space, EActionType::Fire);
		MapKeyToAction(sf::Keyboard::E, EActionType::AltFire);
		MapKeyToAction(sf::Keyboard::Num1, EActionType::Select1);
		MapKeyToAction(sf::Keyboard::Num2, EActionType::Select2);
		MapKeyToAction(sf::Keyboard::Num3, EActionType::Select3);
		MapKeyToAction(sf::Keyboard::Num4, EActionType::Select4);

		return true;
	}

	void InputManager::Shutdown()
	{
		m_keymapping.clear();

		auto callbackIterator = m_actioncallbacks.begin();
		while (callbackIterator != m_actioncallbacks.end())
		{
			std::vector<AbstractActionCallback*>& callbacks = callbackIterator->second;
			auto itr = callbacks.begin();
			while (itr != callbacks.end())
			{
				delete (*itr);
				++itr;
			}
			callbacks.clear();
			++callbackIterator;
		}
		m_actioncallbacks.clear();

		auto mouseCallbackIter = m_mouse_callbacks.begin();
		while (mouseCallbackIter != m_mouse_callbacks.end())
		{
			delete (*mouseCallbackIter);
			++mouseCallbackIter;
		}
		m_mouse_callbacks.clear();
	}

	void InputManager::MapKeyToAction(sf::Keyboard::Key key, EActionType action)
	{
		m_keymapping[key] = action;
	}

	void InputManager::UnregisterKeyActionListener(EActionType action, void* object)
	{
		auto itr = m_actioncallbacks.find(action);
		while (itr != m_actioncallbacks.end())
		{
			auto itri = itr->second.begin();
			while (itri != itr->second.end())
			{
				if ((*itri)->IsObject(object))
				{
					delete (*itri);
					itr->second.erase(itri);
					break;
				}
			}
			++itr;
		}
	}

	void InputManager::UnregisterMouseEventListener(void* object)
	{
		auto itr = m_mouse_callbacks.begin();
		while (itr != m_mouse_callbacks.end())
		{
			if ((*itr)->IsObject(object))
			{
				delete (*itr);
				m_mouse_callbacks.erase(itr);
				break;
			}
			++itr;
		}
	}

	// private
	void InputManager::OnKeyboard(sf::Keyboard::Key key, bool state)
	{
		auto keymapIterator = m_keymapping.find(key);
		if (keymapIterator == m_keymapping.end())
			return;

		const auto& action = keymapIterator->second;
		auto callbackIterator = m_actioncallbacks.find(action);
		if (callbackIterator == m_actioncallbacks.end())
			return;

		const std::vector<AbstractActionCallback*>& callbacks = callbackIterator->second;
		auto itr = callbacks.begin();
		while (itr != callbacks.end())
		{
			(*itr)->OnAction(action, state);
			++itr;
		}
	}

	void InputManager::OnMouse(unsigned short x, unsigned short y, EMouseButton button, bool state)
	{
		MouseEvent event;
		if (button != EMouseButton::MOUSE_BUTTON_UNKNOWN)
		{
			event.m_type = state ? EMouseEventType::MOUSE_EVENT_DOWN : EMouseEventType::MOUSE_EVENT_UP;
			event.m_button = button;
			event.m_state = state;
		}
		else
		{
			event.m_type = EMouseEventType::MOUSE_EVENT_MOVE;
		}
		event.m_x = x;
		event.m_y = y;

		auto itr = m_mouse_callbacks.begin();
		while (itr != m_mouse_callbacks.end())
		{
			(*itr)->OnEvent(event);
			++itr;
		}
	}
} // namespace spaceshooter
