// PlayerControllerComponent.cpp

#include "stdafx.h"
#include "InputManager.hpp"
#include "ServiceLocator.hpp"
#include "PlayerControllerComponent.hpp"

namespace spaceshooter
{
	PlayerControllerComponent::PlayerControllerComponent()
	{
		InputManager* inputManager = ServiceLocator<InputManager>::GetService();
		inputManager->RegisterKeyActionListener(EActionType::Left, this, &PlayerControllerComponent::OnAction);
		inputManager->RegisterKeyActionListener(EActionType::Right, this, &PlayerControllerComponent::OnAction);
		inputManager->RegisterKeyActionListener(EActionType::Up, this, &PlayerControllerComponent::OnAction);
		inputManager->RegisterKeyActionListener(EActionType::Down, this, &PlayerControllerComponent::OnAction);
	}

	PlayerControllerComponent::~PlayerControllerComponent()
	{
		InputManager* inputManager = ServiceLocator<InputManager>::GetService();
		inputManager->UnregisterKeyActionListener(EActionType::Left, this);
		inputManager->UnregisterKeyActionListener(EActionType::Right, this);
		inputManager->UnregisterKeyActionListener(EActionType::Up, this);
		inputManager->UnregisterKeyActionListener(EActionType::Down, this);
	}

	EComponentType PlayerControllerComponent::GetType() const
	{
		return EComponentType::PlayerControllerComponent;
	}

	// private
	void PlayerControllerComponent::OnAction(EActionType type, bool state)
	{
		m_actions[(unsigned int)type] = state;
	}
} // namespace spaceshooter
