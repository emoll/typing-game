// Entity.cpp

#include "stdafx.h"
#include "Entity.hpp"

namespace spaceshooter
{
	Entity::Entity()
	{
		m_components.reserve( static_cast<unsigned int>(EComponentType::Count) );
	}

	Entity::~Entity()
	{
		// note(tommi): delete components
		auto itr = m_components.begin();
		while (itr != m_components.end())
		{
			delete (*itr);
			++itr;
		}
		m_components.clear();
	}

	bool Entity::HasComponent(EComponentType type) const
	{
		for (unsigned int index = 0; index < m_components.size(); index++)
		{
			if (m_components[index]->GetType() == type)
				return true;
		}
		return false;
	}

	void Entity::AddComponent(AbstractComponent* component)
	{
		m_components.push_back(component);
	}
} // namespace spaceshooter
