// GUITestState.cpp

#include "stdafx.h"
#include "GUITestState.hpp"

/* Managers */
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "ServiceLocator.hpp"
#include "GUIManager.hpp"

/* probably not used old gui */
#include "GUIWidget.hpp"
#include "GUILabel.hpp"


/* TextFileToStringConverter stuff */
#include <fstream>
#include <streambuf>
#include <sys/stat.h>

/* TGUI Theme*/
#define THEME_CONFIG_FILE "../external/TGUI/widgets/Black.conf"


namespace spaceshooter
{

	void TextFileToStringConverter::SaveSpeed(float speed)
	{
		std::ofstream ofs(m_wpm_file, std::ofstream::out);

		ofs << std::to_string(speed);

		ofs.close();
	}

	float TextFileToStringConverter::LoadSpeed()
	{
		std::ifstream t(m_wpm_file);
		std::string str;

		t.seekg(0, std::ios::end);
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
		return stof(str);
	}

	TextFileToStringConverter::TextFileToStringConverter()
	{
		Initialize();
	}

	TextFileToStringConverter::~TextFileToStringConverter()
	{
	}


	bool TextFileToStringConverter::DoesFileExist(const std::string& filePath) {
		struct stat buffer;
		return (stat(filePath.c_str(), &buffer) == 0);
	}

	std::string TextFileToStringConverter::GetStringFromFile(const std::string& filePath)
	{
		std::ifstream t(filePath);
		std::string str;

		t.seekg(0, std::ios::end);
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
		return str;
	}

	std::string& TextFileToStringConverter::GetStringAt(int index)
	{
		return m_texts[index];
	}

	void TextFileToStringConverter::Initialize()
	{
		m_wpm_file = "../assets/texts/wpm.txt";

		//go through all text files

		std::stringstream sstm;
		
		for (int i = 0;/*Forever mohahahaha*/; i++)
		{
			sstm << "../assets/texts/" << i << ".txt";
			const std::string filePath = sstm.str(); 
			if (DoesFileExist(filePath))
			{
				std::cout << "Loading file \"" << filePath << "\"" << std::endl;
				m_texts.push_back(GetStringFromFile(filePath));
			}
			else
			{
				m_len = i;
				std::cout << "file \"" << filePath << "\" was not found. No further search will therefore occur." << std::endl;
				break;
			}
			sstm.str("");
		}
	}

	GUITestState::GUITestState()
	{
		m_draw_manager = nullptr;
		m_gui_manager  = nullptr;
		typedText = "";
	}

	GUITestState::~GUITestState()
	{
		m_draw_manager = nullptr;
	}

	TextBox::Ptr GUITestState::CreateReadBox(tgui::Gui& gui)
	{	// Sets the Properties of a read box
		TextBox::Ptr box(gui);
		box->setTextSize(16);
		box->setBorders(8, 10, 8, 10);
		box->setBackgroundColor(sf::Color().White);
		box->setBorderColor(sf::Color().White);
		box->setReadOnly(true); // Disables manual text editing
		box->disable(); // Disables mouse interaction
		return box;
	}

	TextBox::Ptr GUITestState::CreateTypeBox(tgui::Gui& gui)
	{	// Sets the Properties of a read box
		TextBox::Ptr box(gui);
		box->setTextSize(16);
		box->setBorders(8, 10, 8, 10);
		box->setBackgroundColor(sf::Color().White);
		box->setBorderColor(sf::Color(200,200,200));
		return box;
	}

	bool GUITestState::Enter()
	{
		/* TGUI Init */
		m_gui = ServiceLocator<tgui::Gui>::GetService();

		m_raceStarted = false;
		m_initTimer = true;

		/* Create the READING box */
		readBox = CreateReadBox(*m_gui);
		readBox->setPosition(0, 20); // Position
		readBox->setSize(640, 360);    // Size

		/* Create the overlay progress text box */
		overlayBox = CreateReadBox(*m_gui);
		overlayBox->setBackgroundColor(sf::Color(0, 0, 0, 0));
		overlayBox->setTextColor(sf::Color(100,255,100,150));
		overlayBox->setPosition(0, 20); // Position
		overlayBox->setSize(640, 360);    // Size

		/* Load strings */
		sf::String loadedString = m_conv.GetStringAt((int)Random(0, m_conv.m_len));
		readBox->setText(loadedString);
		m_total_words = loadedString.getSize() / 5.f;

		/* Create the typing box */
		typeBox = CreateTypeBox(*m_gui);
		typeBox->setPosition(0, readBox->getSize().y + 20); // Position
		typeBox->setSize(640, 16);    // Size
		typeBox->focus();

		/* Create the progress bar for player*/
		loadingBar = tgui::LoadingBar::Ptr(*m_gui);
		loadingBar->load(THEME_CONFIG_FILE);
		loadingBar->setPosition(0, 416);
		loadingBar->setSize(640, loadingBar->getSize().y);
		loadingBar->setText("You | Start typing to start the race");
		loadingBar->setMaximum(readBox->getText().getSize());

		/* Load saved average typing speed from file */
		m_avg_wpm = m_conv.LoadSpeed();

		/* Create the progress bar for Bot */
		botBar = tgui::LoadingBar::Ptr(*m_gui);
		botBar->load(THEME_CONFIG_FILE);
		botBar->setPosition(0, 464);
		botBar->setSize(640, botBar->getSize().y);
		botBar->setText("Bot | wpm " + std::to_string((int)m_avg_wpm));
		botBar->setMaximum(readBox->getText().getSize());
		botBar->setMaximum(1000);


		
		return true;
	}

	void GUITestState::Exit()
	{
		if (m_gui_manager)
		{
			delete m_gui_manager;
			m_gui_manager = nullptr;
		}
	}

	void GUITestState::ResetGame()
	{
		m_gameOver = false;
		m_raceStarted = false;
		m_initTimer = true;
		
		typedText = "";

		m_wpm = 0.f;
		m_minutesPassed = 0.f;
		m_wordsTyped = 0.f;
		m_lastSize = 0;

		/* Load strings */
		sf::String loadedString = m_conv.GetStringAt((int)Random(0, m_conv.m_len));
		readBox->setText(loadedString);
		readBox->setTextColor(sf::Color().Black);
		m_total_words = loadedString.getSize() / 5.f;
		loadingBar->setText("You | Start typing to start the race");
		loadingBar->setMaximum(readBox->getText().getSize());
		loadingBar->setValue(0);
		
		botBar->setText("Bot | wpm " + std::to_string((int)m_avg_wpm));
		botBar->setValue(0);

		/* Load saved average typing speed from file */
		m_avg_wpm = m_conv.LoadSpeed();

		typeBox->setText("");
	}

	bool GUITestState::Update(float deltatime)
	{
		m_gui_manager->Update();

		/* Game over handling */
		if (m_gameOver)
		{
			if (typeBox->getText() == "retry")
			{
				ResetGame();
			}
			return true;
		}


		/* Update you */
		if (typeBox->getText().getSize() != m_lastSize)
		{
			if ((typedText + typeBox->getText()) == readBox->getText().substring(0, (typedText + typeBox->getText()).getSize()))
			{
				m_lastSize = typeBox->getText().getSize();
				if (m_initTimer && typeBox->getText().getSize() > 0)
				{
					m_raceStarted = true;
					clock.restart();
					m_initTimer = false;
				}
				overlayBox->setText(typedText + typeBox->getText());
				overlayBox->setTextColor(sf::Color().Green);
				readBox->setTextColor(sf::Color().Black);
				if (typeBox->getText().find(" ") != sf::String::InvalidPos)
				{
					typedText += typeBox->getText();
					typeBox->setText("");
				}
				if (typedText + typeBox->getText() == readBox->getText() && m_raceStarted)
				{
					m_raceStarted = false;
					m_gameOver = true;
					typedText += typeBox->getText();
					typeBox->setText("");

					/* Share a happy message */
					sf::String happyMessage = "You won!\n";
					happyMessage += "Your average typing speed was " + std::to_string((int)round(m_wpm)) + " words per minute!\n";
					happyMessage += "You typed this text in about ";
					if (floor(m_minutesPassed) != 0)
						happyMessage += std::to_string((int)floor(m_minutesPassed)) + " minutes and ";
					happyMessage += std::to_string((int)round((m_minutesPassed - floor(m_minutesPassed)) * 60.f)) + " seconds!\n";
					happyMessage += "Type \'retry\' to play again!";
					readBox->setText(happyMessage);
					readBox->setTextColor(sf::Color(230,150,00));
					overlayBox->setText("");

					/* Set and save average speed */
					m_avg_wpm = (m_avg_wpm * 3 + m_wpm) / 4;
					m_conv.SaveSpeed(m_avg_wpm);
				}

			}
			else if (m_raceStarted)
			{
				if (typeBox->getText() == "")
					readBox->setTextColor(sf::Color().Black);
				else
					readBox->setTextColor(sf::Color().Red);
				
			}
		}
		

		if (m_raceStarted)
		{
			m_minutesPassed = (clock.getElapsedTime().asSeconds() / 60);
			m_wordsTyped = (.2f * overlayBox->getText().getSize());
			m_wpm = m_wordsTyped / m_minutesPassed;
			loadingBar->setText("Player | " + std::to_string((int)round(m_wpm)) + " wpm");
			loadingBar->setValue(overlayBox->getText().getSize());

			/* Update Bot */
			float botProg = (m_avg_wpm * (clock.getElapsedTime().asSeconds() / 60)) / m_total_words;
			botBar->setValue(botProg * 1000);
			if (botProg >= 1)
			{
				m_raceStarted = false;
				m_gameOver = true;
				typedText += typeBox->getText();
				typeBox->setText("");

				/* Share a sad message */
				sf::String sadMessage = "You lost...\n";
				sadMessage += "Your average typing speed was " + std::to_string((int)round(m_wpm)) + " words per minute!\n";
				sadMessage += "Type \'retry\' to play again!";

				readBox->setText(sadMessage);
				readBox->setTextColor(sf::Color(230, 20, 00));
				overlayBox->setText("");

				/* Set and save average speed */
				m_avg_wpm = (m_avg_wpm * 3 + m_wpm) / 4;
				m_conv.SaveSpeed(m_avg_wpm);
			}
		}

		return true;
	}

	void GUITestState::Draw()
	{
		m_gui->draw();
		m_gui_manager->Draw(m_draw_manager);
	}

	std::string GUITestState::GetNextState()
	{
		return std::string("");
	}

	// private
	void GUITestState::OnAction(EActionType action, bool state)
	{
	}

	void GUITestState::OnMouse(const MouseEvent& event)
	{
	}
} // namespace spaceshooter
