// AbstractComponent.cpp

#include "stdafx.h"
#include "AbstractComponent.hpp"

namespace spaceshooter
{
	// note(tommi): empty virtual destructor
	// so all derived destructors get called
	AbstractComponent::~AbstractComponent()
	{
	}
} // namespace spaceshooter
