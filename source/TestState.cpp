// TestState.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "AudioManager.hpp"
#include "DrawManager.hpp"
#include "TestState.hpp"

namespace spaceshooter
{
	TestState::TestState()
	{
		m_screen_width = 1024.0f;
		m_screen_height = 600.0f;

		for (unsigned int index = 0; index < ACTION_COUNT; index++)
		{
			m_actions[index] = false;
		}

		m_bullet_count = 0;
		m_bullet_speed = 400.0f;
		m_bullet_texture = nullptr;
	}

	TestState::~TestState()
	{
	}

	bool TestState::Enter()
	{
		// note(tommi): register to listen for all input actions
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->RegisterKeyActionListener(EActionType::Left, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Right, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Up, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Down, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Fire, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::AltFire, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select1, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select2, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select3, this, &TestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select4, this, &TestState::OnAction);

		// note(tommi): test sprite for player
		m_player_position = { 300.0f, m_screen_height * 0.5f };
		m_player_velocity = { 0.0f, 0.0f };

		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
		m_player_texture = texture_manager->CreateTextureFromFile("../assets/player.png");
		m_player_texture->setSmooth(false);
		m_player_sprite.setTexture(*m_player_texture);
		m_player_sprite.setPosition(m_player_position);
		m_player_sprite.setOrigin(
			static_cast<float>(m_player_sprite.getTextureRect().width) * 0.5f,
			static_cast<float>(m_player_sprite.getTextureRect().height) * 0.5f);

		// note(tommi): player bullet texture and sprite
		m_bullet_texture = texture_manager->CreateTextureFromFile("../assets/bullet.png");
		m_bullet_texture->setSmooth(false);
		m_bullet_sprite.setTexture(*m_bullet_texture);
		m_bullet_sprite.setOrigin(
			static_cast<float>(m_bullet_sprite.getTextureRect().width) * 0.5f,
			static_cast<float>(m_bullet_sprite.getTextureRect().height) * 0.5f);

		AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();
		sf::SoundBuffer* sound_buffer = audio_manager->CreateSoundFromFile("../assets/audio/gunfire.ogg");
		m_sound.setBuffer(*sound_buffer);
		m_sound.setLoop(false);
		m_sound.setVolume(60.0f);

		// note(tommi): test sprite for enemy and enemy patterns
		m_enemy_texture = texture_manager->CreateTextureFromFile("../assets/enemy.png");
		m_enemy_sprite.setTexture(*m_enemy_texture);
		m_enemy_sprite.setPosition(m_screen_width, m_screen_height * 0.5f);
		m_enemy_sprite.setOrigin(
			static_cast<float>(m_enemy_sprite.getTextureRect().width) * 0.5f,
			static_cast<float>(m_enemy_sprite.getTextureRect().height) * 0.5f);
		m_enemy_total_time = 0.0f;

		// backgrounds
		m_background01_texture = texture_manager->CreateTextureFromFile("../assets/background01.png");
		m_background01_texture->setSmooth(false);
		m_background01_sprite.setTexture(*m_background01_texture);

		m_background02_texture = texture_manager->CreateTextureFromFile("../assets/background02.png");
		m_background02_texture->setSmooth(false);
		m_background02_texture->setRepeated(true);

		m_background03_texture = texture_manager->CreateTextureFromFile("../assets/background03.png");
		m_background03_texture->setSmooth(false);
		m_background03_texture->setRepeated(true);

		m_background04_texture = texture_manager->CreateTextureFromFile("../assets/background04.png");
		m_background04_texture->setSmooth(false);
		m_background04_texture->setRepeated(true);

		// background 02 vertex array
		{
			float screenHeight = 600.0f;
			float backgroundW = static_cast<float>(m_background02_texture->getSize().x);
			float backgroundH = static_cast<float>(m_background02_texture->getSize().y);
			float backgroundY = screenHeight - backgroundH;

			sf::Vertex vertices[] =
			{
				{
					sf::Vector2f(0.0f, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, backgroundH)
				},
				{
					sf::Vector2f(0.0f, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, backgroundH)
				},
			};
			for (unsigned int i = 0; i < 4; i++)
			{
				m_background02_vertex_array.append(vertices[i]);
			}
			m_background02_vertex_array.setPrimitiveType(sf::PrimitiveType::Quads);
		}

		// background 03 vertex array
		{
			float screenHeight = 500.0f;
			float backgroundW = static_cast<float>(m_background03_texture->getSize().x);
			float backgroundH = static_cast<float>(m_background03_texture->getSize().y);
			float backgroundY = screenHeight - backgroundH;

			sf::Vertex vertices[] =
			{
				{
					sf::Vector2f(0.0f, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, backgroundH)
				},
				{
					sf::Vector2f(0.0f, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, backgroundH)
				},
			};
			for (unsigned int i = 0; i < 4; i++)
			{
				m_background03_vertex_array.append(vertices[i]);
			}
			m_background03_vertex_array.setPrimitiveType(sf::PrimitiveType::Quads);
		}

		// background 04 vertex array
		{
			float screenHeight = 500.0f;
			float backgroundW = static_cast<float>(m_background04_texture->getSize().x);
			float backgroundH = static_cast<float>(m_background04_texture->getSize().y);
			float backgroundY = screenHeight - backgroundH;

			sf::Vertex vertices[] =
			{
				{
					sf::Vector2f(0.0f, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, 0.0f),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, 0.0f)
				},
				{
					sf::Vector2f(backgroundW, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(backgroundW, backgroundH)
				},
				{
					sf::Vector2f(0.0f, backgroundH),
					sf::Color(255, 255, 255, 255),
					sf::Vector2f(0.0f, backgroundH)
				},
			};
			for (unsigned int i = 0; i < 4; i++)
			{
				m_background04_vertex_array.append(vertices[i]);
			}
			m_background04_vertex_array.setPrimitiveType(sf::PrimitiveType::Quads);
		}

		// note(tommi): better to have a local pointer than to access it through the service locator
		m_draw_manager = ServiceLocator<DrawManager>::GetService();

		return true;
	}

	void TestState::Exit()
	{
		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
		texture_manager->DestroyTexture(m_background01_texture);
		m_background01_texture = nullptr;
		texture_manager->DestroyTexture(m_background02_texture);
		m_background02_texture = nullptr;
		texture_manager->DestroyTexture(m_background03_texture);
		m_background03_texture = nullptr;
		texture_manager->DestroyTexture(m_background04_texture);
		m_background04_texture = nullptr;
		texture_manager->DestroyTexture(m_player_texture);
		m_player_texture = nullptr;
		texture_manager->DestroyTexture(m_enemy_texture);
		m_enemy_texture = nullptr;

		// note(tommi): we should destroy audio stuff here
		// todo(tommi): add destroy music and soundbuffer methods to the audio manager
		AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();
		// ...

		// note(tommi): unregister all actions listeners
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->UnregisterKeyActionListener(EActionType::Left, this);
		input_manager->UnregisterKeyActionListener(EActionType::Right, this);
		input_manager->UnregisterKeyActionListener(EActionType::Up, this);
		input_manager->UnregisterKeyActionListener(EActionType::Down, this);
		input_manager->UnregisterKeyActionListener(EActionType::Fire, this);
		input_manager->UnregisterKeyActionListener(EActionType::AltFire, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select1, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select2, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select3, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select4, this);
	}

	bool TestState::Update(float deltatime)
	{
		UpdateBackground(deltatime);
		UpdateEnemy(deltatime);
		UpdatePlayer(deltatime);
		UpdatePlayerBullets(deltatime);

		return true;
	}

	void TestState::Draw()
	{
		float y = m_player_position.y;
		float n_y = 1.0f - y / m_screen_height;

		m_draw_manager->Draw(m_background01_sprite, sf::RenderStates::Default);

		{
			sf::RenderStates states;
			states.texture = m_background04_texture;
			states.transform.translate(0.0f, 200.0f + n_y * 150.0f);
			m_draw_manager->Draw(m_background04_vertex_array, states);
		}

		{
			sf::RenderStates states;
			states.texture = m_background03_texture;
			states.transform.translate(0.0f, 400.0f + n_y * 125.0f);
			m_draw_manager->Draw(m_background03_vertex_array, states);
		}
		{
			sf::RenderStates states;
			states.texture = m_background02_texture;
			states.transform.translate(0.0f, 450.0f + n_y * 100.0f);
			m_draw_manager->Draw(m_background02_vertex_array, states);
		}

		m_draw_manager->Draw(m_enemy_sprite, sf::RenderStates::Default);

		for (unsigned int index = 0; index < m_bullet_count; index++)
		{
			sf::RenderStates state;
			state.transform.translate(m_bullet_positions[index]);
			m_draw_manager->Draw(m_bullet_sprite, state);
		}

		m_draw_manager->Draw(m_player_sprite, sf::RenderStates::Default);
	}

	std::string TestState::GetNextState()
	{
		return std::string("");
	}

	// private
	void TestState::OnAction(EActionType action, bool state)
	{
		// note(tommi): map actions to an internal structure 
		m_actions[(unsigned int)action] = state;
	}

	// private
	void TestState::UpdateBackground(float deltatime)
	{
		// note(tommi): this will not work forever since floating point numbers are going to break
		for (unsigned int index = 0; index < 4; index++)
		{
			m_background04_vertex_array[index].texCoords.x += deltatime *  50.0f;
			m_background03_vertex_array[index].texCoords.x += deltatime * 100.0f;
			m_background02_vertex_array[index].texCoords.x += deltatime * 150.0f;
		}
	}

	void TestState::UpdateEnemy(float deltatime)
	{
		const float enemyHalfWidth = m_enemy_texture->getSize().x * 0.5f;
		const float enemyHalfHeight = m_enemy_texture->getSize().y * 0.5f;
		const float enemyScreenMaxX = m_screen_width - enemyHalfWidth;
		const float enemyScreenMaxY = m_screen_height - enemyHalfHeight;
		const float enemyMovementSpeed = 250.0f;
		m_enemy_total_time += deltatime;

		sf::Vector2f movementDirection(-1.0f, 0.0f);
		sf::Vector2f enemyPosition = m_enemy_sprite.getPosition();
		enemyPosition += movementDirection * deltatime * enemyMovementSpeed;
		if (enemyPosition.x < -enemyHalfWidth)
		{
			enemyPosition.x = m_screen_width + enemyHalfWidth;
		}

		m_enemy_sprite.setPosition(enemyPosition);
	}

	void TestState::UpdatePlayer(float deltatime)
	{
		// note(tommi): these will not be hardcoded
		const float playerHalfWidth = m_player_texture->getSize().x * 0.5f;
		const float playerHalfHeight = m_player_texture->getSize().y * 0.5f;
		const float screenWidth = m_screen_width - playerHalfWidth;
		const float screenHeight = m_screen_height - playerHalfHeight;
		const float playerSpeed = 400.0f;

		bool playerMove = false;
		sf::Vector2f playerAcceleration = { 0.0f, 0.0f };

		if (m_actions[ACTION_UP])
		{
			playerMove = true;
			if (m_player_position.y > playerHalfHeight)
			{
				playerAcceleration.y = -1.0f;
			}
		}
		if (m_actions[ACTION_DOWN])
		{
			playerMove = true;
			if (m_player_position.y < screenHeight)
			{
				playerAcceleration.y = 1.0f;
			}
		}
		if (m_actions[ACTION_LEFT])
		{
			playerMove = true;
			if (m_player_position.x > playerHalfWidth)
			{
				playerAcceleration.x = -1.0f;
			}
		}
		if (m_actions[ACTION_RIGHT])
		{
			playerMove = true;
			if (m_player_position.x < screenWidth)
			{
				playerAcceleration.x = 1.0f;
			}
		}

		// note(tommi): calculate rotation
		m_player_direction.y *= (1.0f - deltatime * 4.0f);
		m_player_direction.y += playerAcceleration.y * deltatime;
		m_player_direction.x = 1.0f;
		
		sf::Vector2f direction = m_player_direction;
		Normalize(direction);
		sf::Vector2f left(0.0f, 1.0f);
		float angle = Dot(left, direction);
		float degrees = ToDegrees(atanf(angle));
		m_player_sprite.setRotation(degrees);

		// note(tommi): calculate physics 
		// note(tommi): m/s^2
		Normalize(playerAcceleration);
		playerAcceleration *= playerSpeed;

		// note(tommi): drag in space, really?
		playerAcceleration += m_player_velocity * (playerMove ? -1.0f : -3.5f);

		// note(tommi): p' = �at^2 + vt + p
		m_player_position = 0.5f * playerAcceleration * deltatime * deltatime +
			m_player_velocity * deltatime +
			m_player_position;

		// note(tommi): at + v
		m_player_velocity = playerAcceleration * deltatime + m_player_velocity;

		if (Limit(m_player_position.x, playerHalfWidth, screenWidth))
		{
			m_player_velocity.x = 0.0f;
		}
		if (Limit(m_player_position.y, playerHalfHeight, screenHeight))
		{
			m_player_velocity.y = 0.0f;
		}

		m_player_sprite.setPosition(m_player_position);
	}

	void TestState::UpdatePlayerBullets(float deltatime)
	{
		// note(tommi): we have to spam to fire bullets
		if (m_actions[ACTION_FIRE])
		{
			m_actions[ACTION_FIRE] = false;
			SpawnPlayerBullet(m_player_position);
 			m_sound.play();
		}

		const sf::Vector2f direction(1.0f, 0.0f);
		for (unsigned int index = 0; index < m_bullet_count; index++)
		{
			m_bullet_positions[index] += direction * m_bullet_speed * deltatime;
		}

		for (unsigned int index = 0; index < m_bullet_count;)
		{
			if (m_bullet_positions[index].x > m_screen_width)
			{
				DespawnPlayerBullet(index);
			}
			else
			{
				index++;
			}
		}
	}

	void TestState::SpawnPlayerBullet(const sf::Vector2f& position)
	{
		// note(tommi): if this happens we probably need more bullets
		if (m_bullet_count == MAX_PLAYER_BULLETS)
		{
			return;
		}

		m_bullet_positions[m_bullet_count++] = position;
	}

	void TestState::DespawnPlayerBullet(unsigned int index)
	{
		if (m_bullet_count == 0)
		{
			return;
		}
		else if (m_bullet_count == 1)
		{
			m_bullet_count--;
		}
		else if (index == (m_bullet_count - 1))
		{
			m_bullet_count--;
		}
		else
		{
			std::swap(m_bullet_positions[index], m_bullet_positions[--m_bullet_count]);
		}
	}
} // namespace spaceshooter
