// main.cpp

#include "stdafx.h"
#include "Engine.hpp"
#include <iostream>
#include <windows.h>

int main(int argc, char* argv[])
{
	/* I didn't realize how to remove the console so this was how I did it :( */
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);

	spaceshooter::Engine engine;
	if (engine.Initialize())
		engine.Run();
	engine.Shutdown();
	return 0;
}
