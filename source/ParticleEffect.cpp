// ParticleEffect.cpp

#include "stdafx.h"
#include "ParticleEmitter.hpp"
#include "ParticleEffect.hpp"

namespace spaceshooter
{
	ParticleEffect::ParticleEffect()
	{
		m_update_rate = 1.0f / 30.0f;
		m_timer = 0.0f;
	}

	ParticleEffect::~ParticleEffect()
	{
		auto itr = m_particle_emitters.begin();
		while (itr != m_particle_emitters.end())
		{
			delete (*itr);
			++itr;
		}
		m_particle_emitters.clear();
	}

	void ParticleEffect::Update(float deltatime)
	{
		m_timer += deltatime;
		if (m_timer < m_update_rate)
			return;

		m_timer -= m_update_rate;

		for (unsigned int index = 0; index < m_particle_emitters.size(); index++)
		{
			m_particle_emitters[index]->Update(m_update_rate);
		}
	}

	void ParticleEffect::Draw()
	{
		for (unsigned int index = 0; index < m_particle_emitters.size(); index++)
		{
			m_particle_emitters[index]->Draw();
		}
	}

	void ParticleEffect::AddParticleEmitter(ParticleEmitter* emitter)
	{
		m_particle_emitters.push_back(emitter);
	}

	float ParticleEffect::GetUpdateRate() const
	{
		return m_update_rate;
	}

	void ParticleEffect::SetUpdateRate(float update_rate)
	{
		m_update_rate = update_rate;
	}
} // namespace spaceshooter
