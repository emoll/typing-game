// CollisionTestState.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "ServiceLocator.hpp"
#include "CollisionTestState.hpp"

namespace spaceshooter
{
	CollisionTestState::CollisionTestState()
	{
		m_screen_width = 1024.0f;
		m_screen_height = 600.0f;

		m_draw_manager = nullptr;
		for (unsigned int index = 0; index < ACTION_COUNT; index++)
		{
			m_action[index] = false;
		}
	}

	CollisionTestState::~CollisionTestState()
	{
	}

	bool CollisionTestState::Enter()
	{
		printf("WASD to move circle.\n");
		printf("The line between the circle and box is the closest distance.\n");
		printf("If the circle overlaps the box, the line becomes red.\n");

		// note(tommi): register for actions
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->RegisterKeyActionListener(EActionType::Left, this, &CollisionTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Right, this, &CollisionTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Up, this, &CollisionTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Down, this, &CollisionTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Fire, this, &CollisionTestState::OnAction);
		input_manager->RegisterMouseEventListener(this, &CollisionTestState::OnMouse);

		// note(tommi): get draw manager
		m_draw_manager = ServiceLocator<DrawManager>::GetService();

		m_aabb.SetCenter(sf::Vector2f(200.0f, 200.0f));
		m_aabb.SetExtent(sf::Vector2f(100.0f, 50.0f));
		m_aabb_shape.setSize(m_aabb.GetExtent() * 2.0f);
		m_aabb_shape.setOrigin(m_aabb.GetExtent());
		m_aabb_shape.setPosition(m_aabb.GetCenter());
		m_aabb_shape.setFillColor(sf::Color::Transparent);
		m_aabb_shape.setOutlineThickness(1.0f);
		m_aabb_shape.setOutlineColor(sf::Color::Green);

		m_circle.SetCenter(sf::Vector2f(m_screen_width * 0.5f, m_screen_height * 0.25f));
		m_circle.SetRadius(40.0f);
		m_circle_shape.setRadius(m_circle.GetRadius());
		m_circle_shape.setOrigin(m_circle.GetRadius(), m_circle.GetRadius());
		m_circle_shape.setPosition(m_circle.GetCenter());
		m_circle_shape.setFillColor(sf::Color::Transparent);
		m_circle_shape.setOutlineThickness(1.0f);
		m_circle_shape.setOutlineColor(sf::Color::Cyan);

		m_obb.SetCenter(sf::Vector2f(600.0f, 200.0f));
		m_obb.SetExtent(sf::Vector2f(60.0f, 20.0f));
		m_obb.SetRadians(ToRadians(15.0f));
		m_obb_shape.setPrimitiveType(sf::LinesStrip);
		m_obb_shape.resize(5);

		m_line_circle_aabb.setPrimitiveType(sf::Lines);
		m_line_circle_aabb.resize(2);

		m_line_circle_obb.setPrimitiveType(sf::Lines);
		m_line_circle_obb.resize(2); 

		// line segments
		m_line_A.SetStart(sf::Vector2f(0.0f, 0.0f));
		m_line_A.SetEnd(sf::Vector2f(100.0f, 0.0f));
		m_line_A_vertex_array.resize(2);
		m_line_A_vertex_array.setPrimitiveType(sf::Lines);

		m_line_B.SetStart(sf::Vector2f(400.0f, 400.0f));
		m_line_B.SetEnd(sf::Vector2f(500.0f, 500.0f));
		m_line_B_vertex_array.resize(2);
		m_line_B_vertex_array.setPrimitiveType(sf::Lines);

		return true;
	}

	void CollisionTestState::Exit()
	{
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		input_manager->UnregisterKeyActionListener(EActionType::Left, this);
		input_manager->UnregisterKeyActionListener(EActionType::Right, this);
		input_manager->UnregisterKeyActionListener(EActionType::Up, this);
		input_manager->UnregisterKeyActionListener(EActionType::Down, this);
		input_manager->UnregisterKeyActionListener(EActionType::Fire, this);
		input_manager->UnregisterMouseEventListener(this);
	}

	bool CollisionTestState::Update(float deltatime)
	{
		sf::Vector2f direction;
		if (m_action[ACTION_LEFT])
			direction.x = -1.0f;
		if (m_action[ACTION_RIGHT])
			direction.x = 1.0f;
		if (m_action[ACTION_UP])
			direction.y = -1.0f;
		if (m_action[ACTION_DOWN])
			direction.y = 1.0f;

		Normalize(direction);
		float length = Length(direction);
		if (length > 0.0f)
		{
			sf::Vector2f center = m_circle.GetCenter();
			center += direction * deltatime * 200.0f;
			m_circle.SetCenter(center);
			m_circle_shape.setPosition(m_circle.GetCenter());
		}

		// note(tommi): closest point on box to circle
		float boxMinX = m_aabb.GetCenter().x - m_aabb.GetExtent().x;
		float boxMaxX = m_aabb.GetCenter().x + m_aabb.GetExtent().x;
		float boxMinY = m_aabb.GetCenter().y - m_aabb.GetExtent().y;
		float boxMaxY = m_aabb.GetCenter().y + m_aabb.GetExtent().y;

		float boxX = Clamp(m_circle.GetCenter().x, boxMinX, boxMaxX);
		float boxY = Clamp(m_circle.GetCenter().y, boxMinY, boxMaxY);

		sf::Vector2f boxClosestPoint(boxX, boxY);
		sf::Vector2f boxCenterToCircleCenter = boxClosestPoint - m_circle.GetCenter();
		float distance = Length(boxCenterToCircleCenter);

		// note(tommi): this is the line between the closest point 
		// on the box and the center of the circle
		sf::Color lineColor = sf::Color::Yellow;
		if (distance <= m_circle.GetRadius())
			lineColor = sf::Color::Red;

		m_line_circle_aabb[0].position = boxClosestPoint;
		m_line_circle_aabb[0].color = lineColor;
		m_line_circle_aabb[1].position = m_circle.GetCenter();
		m_line_circle_aabb[1].color = lineColor;

		// note(tommi): obb debug visual construction
		for (unsigned int i = 0; i < 5; i++)
		{
			m_obb_shape[i].position = m_obb.GetCorner(i % 4);
			m_obb_shape[i].color = sf::Color::Green;
		}

		// note(tommi): we wan't to construct a line just like with the circle and aabb
		// but instead with the circle and obb
		// here we simplify the problem by rotating the circle by the negative radians
		// of the obb, so we can easily compare obb (simplified to a aabb) and circle
		float fC = cosf(m_obb.GetRadians());
		float fS = sinf(m_obb.GetRadians());

		sf::Vector2f circleObbCenterDiff = m_circle.GetCenter() - m_obb.GetCenter();

		// note(tommi): rotate circle center 
		sf::Vector2f circleRotatedCenter;
		circleRotatedCenter.x = circleObbCenterDiff.x * fC + circleObbCenterDiff.y * fS;
		circleRotatedCenter.y = -circleObbCenterDiff.x * fS + circleObbCenterDiff.y * fC;

		// note(tommi): now we can construct the obb as an aabb
		boxMinX = -m_obb.GetExtent().x;
		boxMaxX = m_obb.GetExtent().x;
		boxMinY = -m_obb.GetExtent().y;
		boxMaxY = m_obb.GetExtent().y;

		boxX = Clamp(circleRotatedCenter.x, boxMinX, boxMaxX);
		boxY = Clamp(circleRotatedCenter.y, boxMinY, boxMaxY);

		sf::Vector2f obbClosestPoint(boxX, boxY);
		// note(tommi): rotate the aabb point to a obb point
		obbClosestPoint.x = obbClosestPoint.x * fC - obbClosestPoint.y * fS;
		obbClosestPoint.y = obbClosestPoint.x * fS + obbClosestPoint.y * fC;

		distance = Length((obbClosestPoint + m_obb.GetCenter()) - m_circle.GetCenter());

		// note(tommi): the actual line between the closest point on the obb and circle
		lineColor = sf::Color::Yellow;
		if (distance <= m_circle.GetRadius())
			lineColor = sf::Color::Red;

		m_line_circle_obb[0].position = obbClosestPoint + m_obb.GetCenter();
		m_line_circle_obb[0].color = lineColor;
		m_line_circle_obb[1].position = m_circle.GetCenter();
		m_line_circle_obb[1].color = lineColor;

		// line segments
		lineColor = sf::Color::Green;
		if (Overlap(m_line_A, m_line_B))
		{
			lineColor = sf::Color::Red;
		}
		m_line_A_vertex_array[0].position = m_line_A.GetStart();
		m_line_A_vertex_array[0].color = lineColor;
		m_line_A_vertex_array[1].position = m_line_A.GetEnd();
		m_line_A_vertex_array[1].color = lineColor;

		m_line_B_vertex_array[0].position = m_line_B.GetStart();
		m_line_B_vertex_array[0].color = lineColor;
		m_line_B_vertex_array[1].position = m_line_B.GetEnd();
		m_line_B_vertex_array[1].color = lineColor;

		return true;
	}

	void CollisionTestState::Draw()
	{
		m_draw_manager->Draw(m_aabb_shape, sf::RenderStates::Default);
		m_draw_manager->Draw(m_circle_shape, sf::RenderStates::Default);
		m_draw_manager->Draw(m_obb_shape, sf::RenderStates::Default);
		m_draw_manager->Draw(m_line_circle_aabb, sf::RenderStates::Default);
		m_draw_manager->Draw(m_line_circle_obb, sf::RenderStates::Default); 

		m_draw_manager->Draw(m_line_A_vertex_array);
		m_draw_manager->Draw(m_line_B_vertex_array);
	}

	std::string CollisionTestState::GetNextState()
	{
		return std::string("");
	}

	// private
	void CollisionTestState::OnAction(EActionType action, bool state)
	{
		m_action[(unsigned int)action] = state;
	}

	void CollisionTestState::OnMouse(const MouseEvent& event)
	{
		if (event.GetType() == EMouseEventType::MOUSE_EVENT_MOVE)
		{
			m_line_A.SetStart(sf::Vector2f(event.GetX(), event.GetY()));
			m_line_A.SetEnd(sf::Vector2f(event.GetX() + 100.0f, event.GetY()));
		}
	}
} // namespace spaceshooter
