// ParticleEffectManager.hpp

#ifndef PARTICLEEMITTERMANAGER_HPP_INCLUDED
#define PARTICLEEMITTERMANAGER_HPP_INCLUDED

namespace spaceshooter
{
	class ParticleEffect;

	class ParticleEffectManager
	{
	public:
		ParticleEffectManager();
		~ParticleEffectManager();

		void Update(float deltatime);
		void Draw();

		void AttachParticleEffect(ParticleEffect* effect);
		void DetachParticleEffect(ParticleEffect* effect);

	private:
		std::vector<ParticleEffect*> m_particle_effects;
	};
} // namespace spaceshooter

#endif // PARTICLEEMITTERMANAGER_HPP_INCLUDED
