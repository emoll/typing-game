// Entity.hpp

#ifndef ENTITY_HPP_INCLUDED
#define ENTITY_HPP_INCLUDED

#include "AbstractComponent.hpp"

namespace spaceshooter
{
	class Entity
	{
	public:
		Entity();
		~Entity();

		bool HasComponent(EComponentType type) const;
		void AddComponent(AbstractComponent* component);

		template <class T>
		T* GetComponent(EComponentType type)
		{
			for (unsigned int index = 0; index < m_components.size(); index++)
			{
				if (m_components[index]->GetType() == type)
				{
#if defined(NDEBUG)
					return static_cast<T*>(m_components[index]);
#else
					// note(tommi): dynamic cast should generally be avoided
					// we only use it in debug code to catch errors early
					T* component = dynamic_cast<T*>(m_components[index]);
					assert(component != nullptr);
					return component;
#endif
				}
			}
			return nullptr;
		}

	private:
		std::vector<AbstractComponent*> m_components;
	};
} // namespace spaceshooter

#endif // ENTITY_HPP_INCLUDED
