// PlayerControllerComponent.hpp

#ifndef PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED
#define PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED

#include "AbstractComponent.hpp"

namespace spaceshooter
{
	class PlayerControllerComponent : public AbstractComponent
	{
	public:
		PlayerControllerComponent();
		~PlayerControllerComponent();

		EComponentType GetType() const;

	private:
		void OnAction(EActionType type, bool state);

	private:
		bool m_actions[(unsigned int)EActionType::Count];
	};
} // namespace spaceshooter

#endif // PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED
