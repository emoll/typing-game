// PathfindingTestState.hpp

#ifndef PATHFINDINGTESTSTATE_HPP_INCLUDED
#define PATHFINDINGTESTSTATE_HPP_INCLUDED

#include "AbstractState.hpp"

namespace spaceshooter
{
	class DrawManager;

	class PathfindingTestState : public AbstractState
	{
	public:
		PathfindingTestState();
		~PathfindingTestState();

		bool Enter();
		void Exit();
		bool Update(float deltatime);
		void Draw();
		std::string GetNextState();

	private:
		void OnAction(EActionType action, bool state);
		void OnMouse(const MouseEvent& event);
		void UpdateNodeColor(unsigned int index);

		struct GridNode;

		void SearchPath();
		void AddNodeToList(std::vector<GridNode*>& list, GridNode* node);
		void RemoveNodeFromList(std::vector<GridNode*>& list, GridNode* node);
		bool IsNodeInList(const std::vector<GridNode*>& list, GridNode* node);
		GridNode* FindLowestScoreInOpenList();
		GridNode* GetNodeAt(unsigned int x, unsigned int y);
		float CalculateHeuristics(int x, int y);
		void ClearNodes();
		void ResetStartAndGoal();

	private:
		DrawManager* m_draw_manager;
		float m_screen_width;
		float m_screen_height;

		// note(tommi): grid for a-star
		enum ENodeType
		{
			NODE_TYPE_PASSABLE,
			NODE_TYPE_NON_PASSABLE,
			NODE_TYPE_PATH,
			NODE_TYPE_START,
			NODE_TYPE_GOAL,
			NODE_TYPE_COUNT,
		};

		struct GridNode
		{
			ENodeType m_type;
			unsigned short m_x;
			unsigned short m_y;
			float m_F;
			float m_H;
			float m_G;
			GridNode* m_parent;
		};

		unsigned int m_grid_width;
		unsigned int m_grid_height;
		float m_node_square;
		float m_node_offset_x;
		float m_node_offset_y;
		int m_start_index;
		int m_goal_index;
		sf::Color m_node_color[NODE_TYPE_COUNT];
		std::vector<GridNode> m_grid_nodes;
		sf::VertexArray m_node_vertex_array;
		sf::VertexArray m_grid_vertex_array;

		std::vector<GridNode*> m_open_list;
		std::vector<GridNode*> m_closed_list;

		// note(tommi): keyboard input
		bool m_action[(unsigned int)EActionType::Count];

		// note(tommi): mouse input
		typedef sf::Vector2<unsigned short> Vector2us;
		Vector2us m_mouse_up;
		EMouseButton m_mouse_button;
	};
} // namespace spaceshooter

#endif // PATHFINDINGTESTSTATE_HPP_INCLUDED
