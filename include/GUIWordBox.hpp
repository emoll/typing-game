// GUIWordBox.hpp

#ifndef GUIWORDBOX_HPP_INCLUDED
#define GUIWORDBOX_HPP_INCLUDED

#include "GUIWidget.hpp"

namespace spaceshooter
{
	class GUIWordBox : public GUIWidget
	{
	public:
		GUIWordBox(sf::Font* font);
		virtual ~GUIWordBox();

		virtual void OnDraw(sf::VertexArray& vertex_array);

		void SetTextString(const std::string& text);
		void SetTextColor(const sf::Color& color);
		void SetTextSize(unsigned int size);
		void SetTextPosition(float x, float y);
		void SetTextPosition(const sf::Vector2f& position);

		const sf::Text& GetText() const;

	private:
		sf::Text m_text;
	};
} // namespace spaceshooter

#endif // GUIWORDBOX_HPP_INCLUDED
