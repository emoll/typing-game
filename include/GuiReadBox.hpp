// GUIReadBox.hpp
#ifndef GUIREADBOX_HPP_INCLUDED
#define GUIREADBOX_HPP_INCLUDED

#include "stdafx.h"
namespace spaceshooter
{
	class GUIReadBox : public SFGUI::TextBox::ptr
	{
	public:
		GUIReadBox();
		~GUIReadBox();

	};
} // namespace spaceshooter

#endif // GUIREADBOX_HPP_INCLUDED
