// FontManager.hpp

#ifndef FONTMANAGER_HPP_INCLUDED
#define FONTMANAGER_HPP_INCLUDED

namespace spaceshooter
{
	class FontManager
	{
	public:
		FontManager();
		~FontManager();

		sf::Font* CreateFontFromFile(const std::string& filename);

	private:
		std::map<std::string, sf::Font*> m_fonts;
	};
} // namespace spaceshooter

#endif // FONTMANAGER_HPP_INCLUDED
