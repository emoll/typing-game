// FireParticleEmitter.hpp

#ifndef FIREPARTICLEEMITTER_HPP_INCLUDED
#define FIREPARTICLEEMITTER_HPP_INCLUDED

#include "ParticleEmitter.hpp"

namespace spaceshooter
{
	class DrawManager;

	class FireParticleEmitter : public ParticleEmitter
	{
	public:
		FireParticleEmitter(sf::Texture* texture);
		~FireParticleEmitter();

		void Update(float deltatime);
		void Draw();

	private:
		void SpawnParticle();
		void DespawnParticle(unsigned int index);

	private:
		enum { MAX_PARTICLE_COUNT = 32 };
		struct ParticleTransform
		{
			sf::Vector2f m_position;
			sf::Vector2f m_direction;
		};

		float m_spawn_rate;
		float m_spawn_timer;
		float m_max_age;
		float m_particle_speed;
		unsigned int m_particle_count;
		float m_ages[MAX_PARTICLE_COUNT];
		ParticleTransform m_transforms[MAX_PARTICLE_COUNT];

		sf::VertexArray m_vertex_array;
		DrawManager* m_draw_manager;
	};
} // namespace spaceshooter

#endif // FIREPARTICLEEMITTER_HPP_INCLUDED
