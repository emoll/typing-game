// ParticleEffect.hpp

#ifndef PARTICLEEFFECT_HPP_INCLUDED
#define PARTICLEEFFECT_HPP_INCLUDED

namespace spaceshooter
{
	class ParticleEmitter;
	
	class ParticleEffect
	{
	public:
		ParticleEffect();
		virtual ~ParticleEffect();

		virtual void Update(float deltatime);
		void Draw();

		void AddParticleEmitter(ParticleEmitter* emitter);

		float GetUpdateRate() const;
		void SetUpdateRate(float update_rate);

	protected:
		float m_update_rate;
		float m_timer;
		std::vector<ParticleEmitter*> m_particle_emitters;
	};
} // namespace spaceshooter

#endif // PARTICLEEFFECT_HPP_INCLUDED
