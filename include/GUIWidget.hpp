// GUIWidget.hpp

#ifndef GUIWIDGET_HPP_INCLUDED
#define GUIWIDGET_HPP_INCLUDED

namespace spaceshooter
{
	enum class EWidgetType : unsigned int
	{
		Widget,
		Label,
		WordBox,
		Button,
		Count,
	};

	class GUIWidget
	{
		class AbstractWidgetCallback
		{
		public:
			virtual ~AbstractWidgetCallback();
			virtual void OnChange(const GUIWidget*) = 0;
			virtual bool IsObject(void*) = 0;
		};

		template <class T>
		class WidgetCallback : public AbstractWidgetCallback
		{
		public:
			WidgetCallback(T* object, void(T::*method)(const GUIWidget*))
				: m_object(object)
				, m_method(method)
			{
			}

			void OnChange(const GUIWidget* widget)
			{
				(*m_object.*m_method)(widget);
			}

			bool IsObject(void* object)
			{
				return m_object == object;
			}

		private:
			T* m_object;
			void(T::*m_method)(const GUIWidget*);
		};

	public:
		GUIWidget(EWidgetType type);
		virtual ~GUIWidget();

		virtual void OnDraw(sf::VertexArray& vertex_array) = 0;

		EWidgetType GetType() const;
		bool IsVisible() const;
		bool HasText() const;

		const sf::Rect<float>& GetRect() const;
		void SetRect(const sf::Rect<float>& rect);

		void UnregisterWidgetCallback(void* object);
		template <class T> 
		void RegisterWidgetCallback(T* object, void(T::*method)(const GUIWidget*))
		{
			m_callbacks.push_back(new WidgetCallback<T>(object, method));
		}

	protected:
		void Notify();

	protected:
		EWidgetType m_type;
		bool m_visible;
		bool m_has_text;
		sf::Rect<float> m_rect;
		std::list<AbstractWidgetCallback*> m_callbacks;
	};
} // namespace spaceshooter

#endif // GUIWIDGET_HPP_INCLUDED
