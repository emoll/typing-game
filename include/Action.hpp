// Action.hpp

#ifndef ACTION_HPP_INCLUDED
#define ACTION_HPP_INCLUDED

namespace spaceshooter
{
	enum class EActionType : unsigned int
	{
		Left,
		Right,
		Up,
		Down,
		Fire,
		AltFire,
		Select1,
		Select2,
		Select3,
		Select4,
		Count,
	};
} // namespace spaceshooter

#endif // ACTION_HPP_INCLUDED
