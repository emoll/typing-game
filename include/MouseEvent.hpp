// MouseEvent.hpp

#ifndef MOUSEEVENT_HPP_INCLUDED
#define MOUSEEVENT_HPP_INCLUDED

namespace spaceshooter
{
	// note(tommi): c++11 enum class for stronger type safety
	enum class EMouseEventType : unsigned int
	{
		MOUSE_EVENT_UNKNOWN,
		MOUSE_EVENT_MOVE,
		MOUSE_EVENT_DOWN,
		MOUSE_EVENT_UP,
	};

	enum class  EMouseButton : unsigned short
	{
		MOUSE_BUTTON_LEFT,
		MOUSE_BUTTON_MIDDLE,
		MOUSE_BUTTON_RIGHT,
		MOUSE_BUTTON_UNKNOWN,
	};

	class MouseEvent
	{
		friend class InputManager;
	public:
		MouseEvent();
		MouseEvent(EMouseEventType type);

		const EMouseEventType GetType() const;
		const float GetDelta() const;
		const unsigned short GetX() const;
		const unsigned short GetY() const;
		const EMouseButton GetButton() const;
		const bool GetButtonState() const;

	private:
		EMouseEventType m_type;
		float m_delta;
		unsigned short m_x;
		unsigned short m_y;
		EMouseButton m_button;
		bool m_state;
	};
} // namespace spaceshooter

#endif // MOUSEEVENT_HPP_INCLUDED
