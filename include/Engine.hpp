// Engine.hpp

#ifndef ENGINE_HPP_INCLUDED
#define ENGINE_HPP_INCLUDED

namespace spaceshooter
{
	/* forward declares */
	class DrawManager;
	class InputManager;
	class StateManager;
	class TextureManager;
	class FontManager;
	class CollisionManager;
	class AudioManager;

	class Engine
	{
	public:
		Engine();
		~Engine();

		bool Initialize();
		void Shutdown();
		void Run();

	private:
		void HandleOSEvents();

	private:
		bool m_activated;
		sf::RenderWindow m_window;


		/* MANAGERS */
		DrawManager*		m_draw_manager;
		InputManager*		m_input_manager;
		TextureManager*		m_texture_manager;
		FontManager*		m_font_manager;
		CollisionManager*	m_collision_manager;
		AudioManager*		m_audio_manager;
		StateManager*		m_state_manager;

		/* USED MANAGERS */
		bool m_use_draw_manager;
		bool m_use_input_manager;
		bool m_use_texture_manager;
		bool m_use_font_manager;
		bool m_use_collision_manager;
		bool m_use_audio_manager;
		bool m_use_state_manager;
		bool m_use_tgui;

		/* Fuck it all TGUI library thing*/
		tgui::Gui* m_gui;
	};
} // namespace spaceshooter

#endif // ENGINE_HPP_INCLUDED
