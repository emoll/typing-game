// GUITestState.hpp

#ifndef GUITESTSTATE_HPP_INCLUDED
#define GUITESTSTATE_HPP_INCLUDED

#include "AbstractState.hpp"

namespace spaceshooter
{
	class DrawManager;
	class GUIManager;

	typedef tgui::TextBox TextBox;


	class TextFileToStringConverter
	{
	public:
		TextFileToStringConverter();
		~TextFileToStringConverter();

	public:
		void Initialize();
		std::string* GetRandomText();
		
		std::string& GetStringAt(int index);

		void SaveSpeed(float speed);
		float LoadSpeed();
		
	public:
		int m_len;

	private:
		std::string GetStringFromFile(const std::string& path);
		inline bool DoesFileExist(const std::string& name);

		std::vector<std::string> m_texts;

	private:
		
		std::string m_wpm_file;
	};

	class ReadBox : public TextBox::Ptr
	{
	public:
		ReadBox(tgui::Gui& gui);
		~ReadBox();
	};

	class GUITestState : public AbstractState
	{
	public:
		GUITestState();
		~GUITestState();

		bool Enter();
		void Exit();
		bool Update(float deltatime);
		void Draw();
		void ResetGame();
		std::string GetNextState();

	private:
		void OnAction(EActionType action, bool state);
		void OnMouse(const MouseEvent& event);
		TextBox::Ptr CreateReadBox(tgui::Gui& gui);
		TextBox::Ptr CreateTypeBox(tgui::Gui& gui);
		

	private:
		DrawManager* m_draw_manager;
		GUIManager* m_gui_manager;
		tgui::Gui* m_gui;

		TextBox::Ptr readBox;
		TextBox::Ptr overlayBox;
		TextBox::Ptr typeBox;
		tgui::LoadingBar::Ptr loadingBar;
		tgui::LoadingBar::Ptr botBar;
		
		sf::Clock clock;

		bool m_raceStarted;
		bool m_initTimer;
		bool m_gameOver;

		float m_avg_wpm;
		float m_wpm;
		float m_minutesPassed;
		float m_wordsTyped;
		float m_total_words;

		int m_lastSize;
		
		TextFileToStringConverter m_conv;

		sf::String typedText;
	};
} // namespace spaceshooter

#endif // GUITESTSTATE_HPP_INCLUDED
