// ComponentTestState.hpp

#ifndef COMPONENTTESTSTATE_HPP_INCLUDED
#define COMPONENTTESTSTATE_HPP_INCLUDED

#include "AbstractState.hpp"

namespace spaceshooter
{
	class DrawManager;
	class Entity;

	class ComponentTestState : public AbstractState
	{
	public:
		ComponentTestState();
		~ComponentTestState();

		bool Enter();
		void Exit();
		bool Update(float deltatime);
		void Draw();
		std::string GetNextState();

	private:
		DrawManager* m_draw_manager;

		Entity* m_player;
	};
} // namespace spaceshooter

#endif // COMPONENTTESTSTATE_HPP_INCLUDED
